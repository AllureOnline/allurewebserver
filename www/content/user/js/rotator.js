$(document).ready(function() {
    var slideCount = $(".slider .slide").size();
    if (slideCount > 0) {
        $(".slider .slide:last").after("<ul class='paging' />");
        for (var i = 1; i <= slideCount; i++) {
            $(".paging").append("<li><span onmouseover='slideTo(" + i + ")'>" + i + "</span></li>");
        }
        $(".paging li:first").addClass('active');
    }
    setInterval('rotator()', 5000);
});
function slideTo(slideNumber) {
    $(".slider .slide").each(function() {
        if ($(this).hasClass('show')) {
            $(this).fadeOut("slow").removeClass("show");
        }
    });
    slideNumber--;
    $(".paging .active").removeClass("active");
    $(".paging li:eq(" + slideNumber + ")").addClass("active");
    $(".slide:eq(" + slideNumber + ")").addClass("show").fadeIn("slow");  
}
function rotator() {
    var slideCount = $(".slider .slide").size();   
    var currentSlide = 1;   
    $(".slider .slide").each(function(i) {
        if ($(this).hasClass('show')) {           
            $(".paging .active").removeClass("active");
            $(".paging li:eq(" + (currentSlide-1) + ")").addClass("active");
            currentSlide = i + 1;         
        }
    });
    slideNumber = currentSlide + 1;
    if (currentSlide < slideCount) {
        slideTo(slideNumber);       
    } else {
        slideTo(1);
    }
}