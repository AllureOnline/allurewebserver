function setDefaultValues()
{// устанавливает назад дефолтные значения на полях, в которые ничего не ввели

	$(".pure").each(function(){
		$(this).val( $(this).attr("default") );
	});
}

function validateForm() {

	$(".pure").val("");

    $('#registrationError').html('Введенные пароли не совпадают.').hide();
    $('.registration .error').remove();
    //$('#loginform').hide();    
    if ($('input[name="mName"]').val() == "") {
        $('#registrationError').html('Вы не ввели Логин.').show();
        $('input[name="mName"]').after('<span class="error"></span>');
        $('input[name="mName"]').focus();
		setDefaultValues();
        return false;
    }
    if ($('input[name="mPassword"]').val() == "") {
        $('#registrationError').html('Вы не ввели Пароль.').show();
        $('input[name="mPassword"]').after('<span class="error"></span>');
        $('input[name="mPassword"]').focus();
		setDefaultValues();
        return false;
    }
    if ($('input[name="mPassword2"]').val() == "") {
        $('#registrationError').html('Вы не повторили Пароль.').show();
        $('input[name="mPassword2"]').after('<span class="error"></span>');
        $('input[name="mPassword2"]').focus();
		setDefaultValues();
        return false;
    }
    if ($('input[name="mEmail"]').val() == "") {
        $('#registrationError').html('Вы не ввели E-mail.').show();
        $('input[name="mEmail"]').after('<span class="error"></span>');
        $('input[name="mEmail"]').focus();
		setDefaultValues();
        return false;
    }
    if (!$('input[name="mAgreement"]').attr('checked')) {
        $('#registrationError').html('Вы не подтвердили согласие с условиями пользовательского соглашения.').show();
        $('input[name="mAgreement"]').focus();
		setDefaultValues();
        return false;
    }
    if ($('input[name="mPassword"]').val() != $('input[name="mPassword2"]').val()) {
        $('#registrationError').html('Введенные пароли не совпадают.').show();
        $('input[name="mPassword2"]').after('<span class="error"></span>');
        $('input[name="mPassword"]').after('<span class="error"></span>');
        $('input[name="mPassword"]').focus();
		setDefaultValues();
        return false;
    }    
    var emailPattern = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
    var loginPattern = "^[a-zA-Z0-9]+$";
    var email = $('input[name="mEmail"]').val();
    var login = $('#mName').val();
    var pass1 = $('#mPassword').val();
    var pass2 = $('#mPassword2').val();
    if (email.match(emailPattern) == null) {
        $('#registrationError').html('Введен неверный e-mail.').show();
        $('input[name="mEmail"]').after('<span class="error"></span>');
        $('input[name="mEmail"]').focus();
		setDefaultValues();
        return false;
    }
    if (login.match(loginPattern) == null) {
        $('#registrationError').html('Логин должен состоять только из цифр и латинских букв.').show();
        $('#mName').after('<span class="error"></span>');
        $('#mName').focus();
		setDefaultValues();
        return false;
    }
    if (pass1.match(loginPattern) == null) {
        $('#registrationError').html('Пароль должен состоять только из цифр и латинских букв.').show();
        $('#mPassword').after('<span class="error"></span>');
        $('#mPassword').focus();
		setDefaultValues();
        return false;
    }    
    if (login.length < 8 || login.length > 20) {
        $('#registrationError').html('Логин должен быть от 8 до 20 символов.').show();
        $('#mName').after('<span class="error"></span>');
        $('#mName').focus();
		setDefaultValues();
        return false;
    }
    if (pass1.length < 9 || pass1.length > 15){
        $('#registrationError').html('Пароль должен быть от 9 до 15 символов.').show();
        $('#mPassword').after('<span class="error"></span>');
        $('#mPassword').focus();
		setDefaultValues();
        return false;
    }
    return true;
}

$(document).ready(function(){

	// фокус и потеря фокуса текстовых полей с подсказками

	$(".autohiding").each( function() {
   		$(this).attr( "default", $(this).val() ).addClass("pure");
	}).focus( function() {
		if ( $(this).hasClass("pure") ) {
			$(this).val("");
			$(this).removeClass("pure");
		}
	}).blur( function() {
    	if ( trim($(this).val()) == "" ) {
       		$(this).val( $(this).attr("default") );
			$(this).addClass("pure");
    	}
	});

});

function ltrim(s)
{// очищает левую сторону строки от лишних символов

	var ptrn = /\s*((\S+\s*)*)/;
	return s.replace(ptrn, "$1");
}

function rtrim(s)
{// очищает правую сторону строки от лишних символов

	var ptrn = /((\s*\S+)*)\s*/;
	return s.replace(ptrn, "$1");
}

function trim(s)
{// очищает строку от лишних символов

	return ltrim(rtrim(s));
}
