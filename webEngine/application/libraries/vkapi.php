<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class vkapi {
	var $api_secret;
	var $app_id;
	var $api_url;
	
	function __construct($params){
		$params = $params[0];
		$this->app_id = $params['app_id'];
		$this->api_secret = $params['api_secret'];
		if (!strstr($params['api_url'], 'http://')) $params['api_url'] = 'http://'.$params['api_url'];
		$this->api_url = $params['api_url'];
	}
	
	function api($method,$params=false) {
		if (!$params) $params = array(); 
		$params['api_id'] = $this->app_id;
		$params['v'] = '3.0';
		$params['method'] = $method;
		$params['timestamp'] = time();
		$params['format'] = 'json';
		$params['random'] = rand(0,10000);
		ksort($params);
		$sig = '';
		foreach($params as $k=>$v) {
			$sig .= $k.'='.$v;
		}
		$sig .= $this->api_secret;
		$params['sig'] = md5($sig);
		$query = $this->api_url.'?'.$this->params($params);
		$res = file_get_contents($query);
		$ConvertResponse = json_decode($res, true);
		return $ConvertResponse['response'][0];
	}
	
	function params($params) {
		$pice = array();
		foreach($params as $k=>$v) {
			$pice[] = $k.'='.urlencode($v);
		}
		return implode('&',$pice);
	}
}