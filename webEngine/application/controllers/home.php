<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index(){
		if($this->input->get('ReturnUrl')){
			$this->session->set_userdata(array('ReturnUrl'=>htmlspecialchars(urlencode($this->input->get('ReturnUrl')))));
		}
		$this->load->view('_header', array('title'=>'Главная страница'));
		$this->load->view('news');
		$this->load->view('_fooder');
	}
	
	public function news(){
		$this->load->view('_header', array('title'=>'Новости'));
		$this->load->view('news');
		$this->load->view('_fooder');
	}
	
	public function download(){
		$this->load->view('_header', array('title'=>'Файлы'));
		$this->load->view('download');
		$this->load->view('_fooder');
	}
	
	public function support(){
		$this->load->view('_header', array('title'=>'Поддержка'));
		$this->load->view('support');
		$this->load->view('_fooder');
	}
	
	public function invitefriend(){
		$this->load->view('_header', array('title'=>'Пригласи друга'));
		$this->load->view('invitefriend');
		$this->load->view('_fooder');
	}
	
	public function shop(){
		$this->load->view('_header', array('title'=>'Поддержка'));
//		$this->load->view('support');
		$this->load->view('_fooder');
	}
	
	public function play(){
		$this->load->helper('url');
		$Account = array();
		if($this->input->cookie('sessionCode')){
			$GetLogin = $this->db->query("SELECT active_session.*, users.* FROM users INNER JOIN active_session ON users.id = active_session.uid WHERE users.sesscode = active_session.session AND sesscode = ?", array($this->input->cookie('sessionCode')));
			if($GetLogin->num_rows() > 0){
				$Account = $GetLogin->row();
			}
		}
		if(!$Account){
			redirect(base_url('/error?ReturnUrl=%2Fplay.html'));
		}
		$this->load->view('play');
	}
	
}