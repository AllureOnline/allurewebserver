<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	function login(){
		if (!$this->input->get('user_account') or strlen($this->input->get('user_account')) < 4 or strlen($this->input->get('user_account')) > 20){
			$Response = array('status'=>'101', 'comment'=>'Неверный логин или пароль.');
			exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode($Response) . ");" : $Response['status']);
		}

		if( preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('user_account')) != $this->input->get('user_account') ){
			$Response = array('status'=>'101', 'comment'=>'Неверный логин или пароль.');
			exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode($Response) . ");" : $Response['status']);
		}
		
		$GetUser = $this->db->query("SELECT * FROM users WHERE name = ? AND sesscode = ?", array($this->input->get('user_account'), $this->input->get('user_password')));
		if ($GetUser->num_rows() == 0){
			$Response = array('status'=>'101', 'comment'=>'Неверный логин или пароль.');
			exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode($Response) . ");" : $Response['status']);
		}
		$GetUser = $GetUser->row_array();
		if($GetUser['block'] > time()){
			$Response = array('status'=>'102', 'comment'=>'Логин заблокирован.');
			exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode($Response) . ");" : $Response['status']);
		}
		$Response = array('status'=>'10', 'comment'=>'Успешная авторизация!');	
		exit($this->input->get('callback') ? preg_replace("/[^A-z0-9-_\.]/", "", $this->input->get('callback')) . "(" . json_encode($Response) . ");" : $Response['status']);
	}

}