<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {
	public function history(){
		$this->load->view('_header', array('title'=>'Об игре'));
		$this->load->view('about/history');
		$this->load->view('_fooder');
	}
	public function features(){
		$this->load->view('_header', array('title'=>'Об игре'));

		$this->load->view('_fooder');
	}
	public function classes(){
		$this->load->view('_header', array('title'=>'Об игре'));
		$this->load->view('about/classes');
		$this->load->view('_fooder');
	}
}