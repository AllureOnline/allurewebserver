<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gameplay extends CI_Controller {
	
	public function vkapp(){
		$VkConfig = array(
			'app_id'=>'3888062',
			'api_url'=>'http://api.vk.com/api.php',
			'api_secret'=>'5gqsKg0q2zk21Y813f8EAID1DAmQWvkL',
		);
		$this->load->library('vkapi', array($VkConfig));

		$Autch = time();

		if($_GET['auth_key'] == md5($VkConfig['app_id'].'_'.$_GET['viewer_id'].'_'.$VkConfig['api_secret'])){
			$getProfile = $this->vkapi->api('getProfiles', array('uids'=>intval($_GET['viewer_id']),'fields'=>'nickname,bdate,sex,city,country'));

			$GetUser = $this->db->query("SELECT * FROM users WHERE name = ?", array("vk" . $getProfile['uid']));
			if(!$GetUser->num_rows()){
				$data = array(
					'name'	=>	"vk" . $getProfile['uid'],
					'password'	=>	md5(substr(md5($VkConfig['app_id'].'_'.$_GET['viewer_id'].'_'.$VkConfig['api_secret']), 0, 6)),
					'email'		=>	"id" . $getProfile['uid'] . "@vkmessenger.com",
				);
				$this->db->insert('users', $data);
				$GetUser = $this->db->query("SELECT * FROM users WHERE name = ?", array("vk" . $getProfile['uid']));
			}
			$GetUser = $GetUser->row_array(); 
			if(md5(substr(md5($VkConfig['app_id'].'_'.$_GET['viewer_id'].'_'.$VkConfig['api_secret']), 0, 6)) != $GetUser['password']){
				echo'<center>Ошибка Авторизации</center>';
				exit;
			}
		}else{
			echo'<center>Ошибка Авторизации</center>';
			exit;
		}

		// Проверяем старую сессию... если он есть то удаляем...
		$Query = $this->db->query("SELECT * FROM active_session WHERE uid = ?", array($GetUser['id']));
		if($Query->num_rows() > 0){
			$this->db->query("DELETE FROM active_session WHERE uid = ?", array($GetUser['id']));
		}

		// А тут мы уже вносим новые данные в БД
		$SessionID = md5($GetUser['name'].$GetUser['password'].$Autch.$this->input->ip_address().$this->input->user_agent());
		$this->db->insert('active_session', array(
				'uid'=>$GetUser['id'],
				'atime'=>$Autch,
				'session'=>$SessionID,
				'uip'=>$this->input->ip_address(),
				'browser'=>$this->input->user_agent()
			)
		);

		// заносим сессию и создаем кукисы...
		$this->db->update('users', array('sesscode'=>$SessionID), "id = '".$GetUser['id']."'");
		$this->input->set_cookie(
			array(
				'name'=>'sessionCode',
				'value'=>$SessionID,
				'expire'=>(time()+604800)
			)
		);

		$PlayerParams = array(
			'username'=>$GetUser['name'],
			'password'=>$SessionID,
			'social'=>true
		);

		$this->load->view('play', $PlayerParams);
	}
	
	public function play(){
		
		$Account = array();
		if($this->input->cookie('sessionCode')){
			$GetLogin = $this->db->query("SELECT active_session.*, users.* FROM users INNER JOIN active_session ON users.id = active_session.uid WHERE users.sesscode = active_session.session AND sesscode = ?", array($this->input->cookie('sessionCode')));
			if($GetLogin->num_rows() > 0){
				$Account = $GetLogin->row();
			}
		}
		if(!$Account){
			redirect(base_url('/error?ReturnUrl=%2Fplay.html'));
		}

		$PlayerParams = array(
			'username'=>$Account->name,
			'password'=>$Account->sesscode,
			'social'=>false
		);

		$this->load->view('play', $PlayerParams);
	}
}