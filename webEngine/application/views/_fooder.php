<?php
function xml2array($XML){
    $XML = trim($XML);
    $returnVal = $XML;
    $emptyTag = '<(.*)/>';
    $fullTag = '<\\1></\\1>';
    $XML = preg_replace ("|$emptyTag|", $fullTag, $XML);
    $matches = array();
    if (preg_match_all('|<(.*)>(.*)</\\1>|Ums', trim($XML), $matches)){
        if (count($matches[1]) > 0) $returnVal = array();
        foreach ($matches[1] as $index => $outerXML){
            $attribute = $outerXML;
            $value = xml2array($matches[2][$index]);
            if (! isset($returnVal[$attribute])) $returnVal[$attribute] = array();
                $returnVal[$attribute][] = $value;
        }
    }
    if (is_array($returnVal)) foreach ($returnVal as $key => $value){
        if (is_array($value) && count($value) == 1 && key($value) === 0){
            $returnVal[$key] = $returnVal[$key][0];
        }
    }
    return $returnVal;
}?></div>

					<div id="sidebar-right">
						<div id="shop">
							<div class="item1">  
								<img src='/content/images/index/shop/item1.png' />
								<a class="JexVisualHyperLinkRedact" id="item1" href="/shop.html" target="_self">Магазин</a>
							</div>
						  
							<div class="item2">  
								<img src='/content/images/index/shop/item2.png' />
								<a class="JexVisualHyperLinkRedact" id="item2" href="http://forum.allureonline.ru/viewforum.php?f=2" target="_blank">Советы по игре</a>
							</div>
						  
							<div class="item3">  
								<img src='/content/images/index/shop/item3.png' />
								<a class="JexVisualHyperLinkRedact" id="item3" href="/invitefriend.html" target="_self">Пригласи друга</a>
							</div>
						  
							<div class="item4">  
								<img src='/content/images/index/shop/item4.png' />
								<a class="JexVisualHyperLinkRedact" id="item4" href="/support.html" target="_self">Поддержка</a>
							</div>
						  
							<div class="item5">  
								<img src='/content/images/index/shop/item5.png' />
								<a class="JexVisualHyperLinkRedact" id="item5" href="#" target="_self">Бонусы</a>
							</div>
						</div>
						<div class="rating" id="rating-right">
							<h2>Тефия<!--, Диона, Рея, Феба, Пандора--></h2>                       
								
							<!-- списки с рейтингом -->
							<script>
							var classes = [["pic-green","Ро"],["pic-blue","Амрит"],["pic-red","Кобо"],["pic-none","Класс не установлен"]];
							function getClasses(id){
								return '<img src="/content/images/index/'+classes[id][0]+'.png" width="22" height="22" title="'+classes[id][1]+'" alt="'+classes[id][1]+'" /> ';
							}
							</script>
							<div class="rating-list" id="rating-list-right">
								<div>
									<ul><?php
									if(file_exists(APPPATH . "cache/R-" . date("dmY") . ".txt")){
										include(APPPATH . "cache/R-" . date("dmY") . ".txt");
									}else{
										$fp = fopen(APPPATH . "cache/R-" . date("dmY") . ".txt", 'w');
										$GetRank = xml2array(file_get_contents("/AllureServer/GameServer/data/top/2.txt"));
										$GetNames = explode(",",$GetRank['app.game.gs.data.HeroRankData']['names']);
										$GetLevel = explode(",",$GetRank['app.game.gs.data.HeroRankData']['levels']);
										for($i=0; $i<10;$i++){
											$GetHero = $this->db->query("SELECT * FROM heroes WHERE name = ?", array($GetNames[$i]));
											$GetHero = $GetHero->row();
											$GetClass = xml2array($GetHero['data']);
											fwrite($fp, '<li><script>document.write(getClasses('.$GetClass['app.game.entity.Hero']['race'].'));</script>'.$GetNames[$i].' ['.$GetLevel[$i].']</li>');
										}
										fclose($fp);
										include(APPPATH . "cache/R-" . date("dmY") . ".txt");
									}
									?></ul>
								</div>
								<div>
									<ul>
										<li><img src="/content/images/index/pic-red.png" width="22" height="22" title="" alt="" /> Gods [0]</li>
										<li><img src="/content/images/index/pic-blue.png" width="22" height="22" title="" alt="" /> Bots [0]</li>
										<li><img src="/content/images/index/pic-green.png" width="22" height="22" title="" alt="" /> Admins [0]</li>
									</ul>
								</div>
							</div>
							<!--// списки с рейтингом -->

							<!-- переключатели рейтинга -->
							<ul class="rating-buttons" id="rating-buttons-right">
								<li class="rb-players selected">Игроки</li>
								<li class="rb-clans">Кланы</li>
							</ul>
							<!--// переключатели рейтинга -->
						</div>
					</div>
				</div>
			</div>		
			<div class="clear"></div>
			<div id="footer">
				<div class="copyright">
					<div style="float:left;margin:2px;"><a href="http://top.mail.ru/jump?from=2458081" target="_blank"><img src="//top-fwz1.mail.ru/counter?id=2458081;t=466;l=1" style="margin-right: 0px;" style="border:0;" height="31" width="88" alt="Рейтинг@Mail.ru: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня" title="Рейтинг@Mail.ru: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня" /></a></div>
					Copyright &copy; 2013-<script>document.write((new Date().getFullYear()));</script> Allure Online Entertainment Inc. All Rights Reserved.
					<div style="float:right;margin:2px;"><a href="http://www.liveinternet.ru/click" target="_blank"><script type="text/javascript">document.write('<img style="margin-right:0px;" src="//counter.yadro.ru/hit?t22.5;r'+escape(document.referrer)+((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+";"+Math.random()+'" alt="LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня" title="LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня" border="0" width="88" height="31">');</script></a></div>
				</div>
			</div>
		</div>
	</div><?php
if($this->session->userdata('ReturnUrl')){
echo'	<div class="none">
		<!-- форма логина в окошке -->
		<div id="loginWindow">
			<form class="lw-form" method="post" action="/user/login">

				<h2>Для доступа к запрашиваемой странице, <br />требуется авторизоваться:</h2>

				<label for="p-s" class="l-p-s">Введите логин:</label>
				<input type="text" class="lw-input" name="login" />
				<div class="clear"></div>

				<label for="p-s" class="l-p-s">Введите пароль:</label>
				<input type="password" class="lw-input" name="password" />
				<div class="clear"></div>

				<div class="lw-submit">
					<input type="hidden" name="ReturnUrl" value="' . base64_encode($this->session->userdata('ReturnUrl')) . '" />
					<input class="img-btn img-btn-ok" type="submit" value="" />
				</div>
			</form>
		</div>
		<!--// форма логина в окошке -->
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			tb_show("", "#TB_inline?height=190&width=480&inlineId=loginWindow", null);
		});
	</script>';
}
	?><script type="text/javascript">
		$(document).ready(function() {
			/************************* <рейтинг> *************************/
			if ($("#rating-left").length) {
				var ratingMutexLeft = true; 
				$("#rating-list-left").cycle({
					fx: "scrollHorz",
					timeout: 0,
					speed: 500,
					after: function() {
					   ratingMutexLeft = true;
					}
				});
				$("#rating-buttons-left li").click(function() {
					if (!$(this).hasClass("selected") && ratingMutexLeft) {
					   ratingMutexLeft = false;
						var index = $("#rating-buttons-left li").index($(this));
						$("#rating-list-left").cycle(index);
						$("#rating-buttons-left .selected").removeClass("selected");
					  $(this).addClass("selected"); 
					}
				});
			}

			if ($("#rating-right").length) {
				var ratingMutexRight = true;
				$("#rating-list-right").cycle({
					fx: "scrollHorz",
					timeout: 0,
					speed: 500,
					after: function() {
					ratingMutexRight = true;
					}
				});
				$("#rating-buttons-right li").click(function() {
				if (!$(this).hasClass("selected") && ratingMutexRight) {
						ratingMutexRight = false;
						var index = $("#rating-buttons-right li").index($(this));
						$("#rating-list-right").cycle(index);
						$("#rating-buttons-right .selected").removeClass("selected");
						$(this).addClass("selected");
					}
				});
			}
			/************************* </рейтинг> *************************/
		}); 
	</script>
<!-- Analytics@Google.com counter -->
<script type="text/javascript">//<![CDATA[
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47550200-1', 'allureonline.ru');
  ga('send', 'pageview');
//]]></script>
<!-- //Analytics@Google.com counter -->
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">//<![CDATA[
var _tmr = _tmr || [];
_tmr.push({id: "2458081", type: "pageView", start: (new Date()).getTime()});
(function (d, w) {
   var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
   ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
   var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window);
//]]></script>
<!-- //Rating@Mail.ru counter -->
</body>
</html>