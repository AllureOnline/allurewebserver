<style>
.leftimg {
	float:left;
	margin-right: 7px;
}
</style>
<div class="c-c-c">
	<div class="c-l-t">
		<div class="c-r-t">
			<div class="c-l-b">
				<div class="c-r-b">
					<div id="container">
						<div class="d_img1">
							<img style="position: absolute; z-index: -1;">
							<img style="position: absolute; z-index: -1;">
							<div class="sl2">
								<div class="breadcrumbs">
									<span class="bc-left"></span>
									<a class="bc-first"  href="/about">Об игре</a>
									<span class="bc-center"></span>
									<span class="bc-second">Классы</span>
									<span class="bc-right"></span>
								</div>
							</div>
						</div>
						<div class="text_div" id="updateDiv">
							<div id="textContainer">
								&nbsp;<br>После гибели Мага-Великана, духи и маги ушли дальше в горы и прекратили сражаться. Желтый император вел людей к миру и процветанию, а Фея Девяти Небес открыла им Божественное учение. Ее учение передавалось людьми из поколения в поколение, но каждый начинал понимать его по-своему. Спустя много веков в учении появилось три основных течения, три основные школы.<br>&nbsp;<table border="0" width="100%"><tr><td><img src="/content/images/about/kobo2.png" alt="" title="" class="leftimg"/>Ученики школы <b>Кобо</b> великолепно владеют искусством фехтования; для них суть Божественного учения – это война. Являясь потомками Желтого Императора, они владеют сложнейшей техникой меча и философией души. Со времен Желтого Императора до них дошли знания о методах покорения животных, техника ближнего боя, умение быть быстрым при атаке и беспощадным при обороне - эти знания накапливались не одним поколением.</td></tr></table><br><br><div style="text-align: center;"><img src="/content/images/about/klas1.png" alt="" title=""/></div><br><div style="text-align: center;"><img src="/content/images/about/niz.png" alt="" title=""/></div><br>&nbsp;<table border="0" width="100%"><tr><td><img src="/content/images/about/amrity2.png" alt="" title="" class="leftimg"/><b>Амриты</b> считают, что Божественное учение учит самосовершенствованию; они искусны в магии и в обращении с амулетами. Амриты - потомки Девы Валеи, они владеют сложнейшей техникой посоха. У амритов есть дар повелевать силами природы и использовать их в своих целях. Со времен жизни Феи Девяти Небес до них дошли знания о самовосстановлении, перемещении по вселенной, атаке с дальнего расстояния, умение накапливать силу.</td></tr></table><br><br><div style="text-align: center;"><img src="/content/images/about/klas2.png" alt="" title=""/></div><br><div style="text-align: center;"><img src="/content/images/about/niz.png" alt="" title=""/></div><br>&nbsp;<table border="0" width="100%"><tr><td><img src="/content/images/about/ro2.png" alt="" title="" class="leftimg"/>Последователи третьей школы, <b>Ро</b>, ценят жизнь такой, какая она есть; их излюбленное оружие – лук. Являются потомками Древних зверей и владеют сложнейшей техникой лука и философией души. Со времен Желтого Императора до них дошли знания о силе ветра, мастерство владения луком и стрелами, техника ближнего боя, умение быть неудержимыми.</td></tr></table><br><br><div style="text-align: center;"><img src="/content/images/about/klas3.png" alt="" title=""/></div><br><div style="text-align: center;"><img src="/content/images/about/niz.png" alt="" title=""/></div><br><br>Между этими тремя школами не утихает соперничество, ведь каждый ученик и наставник считает своим долгом доказать, что именно их школа понимает Божественное учение верно и не допускает прочих толкований божественных слов.
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>