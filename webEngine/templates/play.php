<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<title>Allure Online</title>
<link rel="SHORTCUT ICON" href="/favicon.ico">
<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
<script type="text/javascript">
	function SaveCookie(name,value){
		var time=new Date();
		time.setTime(time.getTime()+30*60*60*1000);
		document.cookie=name+"="+value+";expires="+time.toGMTString();
	}
	function GetCookie(name){
		var arrStr = document.cookie.split(";");
		for(var i = 0;i < arrStr.length;i ++){
			var temp = arrStr[i].split("=");
			if(temp[0] == name) 
				return unescape(temp[1]);
		}
	}
</script>
<link type="text/css" rel="stylesheet" href="/includes/css/client.css" />
<script src="//code.jquery.com/jquery-1.9.0.min.js" type="text/javascript"></script>
<script src="//vk.com/js/api/xd_connection.js" type="text/javascript"></script>
</head>
<body style="margin:0;background-color:#19100B;" >
	<div id="GameContent" style="background-color:#19100B;">
		<div align="center" id="appletDiv" style="width:100%;">
			<APPLET archive="allureclient_<?php echo (isset($_GET['dev']) ? 'dev' : 'v' ) . time(); ?>.jar" code="game.GameApplet.class" id="currentApplet" name="game" style="width:100%;height:100%;">
				<param name="java_arguments" value="-Xmx512M -XX:+ForceTimeHighResolution">
				<param name="config" value="client_v<?php echo time(); ?>.cfg">
				<param name="userName" value="<?php echo $username; ?>">
				<param name="password" value="<?php echo $password; ?>">
			</APPLET>
		</div>
		<div class="line"></div>
		<div class="control">
			<div class="left">
				<a href="javascript:void(0);" onclick="RestartGame();" class="home" title="<?php echo ($social ? "Перезапустить игру" : "Главная страница"); ?>"></a>
				<div class="ge;"></div>
			</div>
			<div class="right">
				<div class="win_choose">
					<a href="javascript:void(0);" onclick="fullScreen();" id="gameScreen" class="b_box" title="Полный Экран"></a>
				</div><?php
				if(!$social){
				echo'<div class="ger"></div>
				<div class="sns">
					<a href="javascript:void(0);" onclick="alert(\'В разработке\');" class="facebook" title="Allure Online на Facebook" target="_blank"></a>
					<a href="javascript:void(0);" onclick="alert(\'В разработке\');" class="twitter" title="Твиттер Allure Online" target="_blank"></a>
					<a href="http://vk.com/allureonlineofficial" class="vk" title="Allure Online в Вконтакте" target="_blank"></a>
				</div>';
				}
			?></div>
		</div>
	</div>
	<script src="/includes/js/fullscreen-api-polyfill.js"></script>
	<script type="text/javascript"> 
		var clientHeight = document.compatMode == "CSS1Compat" ? document.documentElement.clientHeight : document.body.clientHeight;
		var clientWidth = document.compatMode == "CSS1Compat" ? document.documentElement.clientWidth : document.body.clientWidth;
		var screenHeight = screen.height;
		var screenWidth = screen.width;
<?php
if($social){
echo'		VK.init(function() {
			VK.addCallback("onWindowBlur",function(){
				document.getElementById("appletDiv").style.width = "0px";
			});
			VK.addCallback("onWindowFocus",function(){
				document.getElementById("appletDiv").style.width = "100%";
			});
			VK.addCallback("onOrderCancel",function(){
				document.getElementById("appletDiv").style.width = "100%";
			});
			showOrderBox = function() {
				VK.callMethod("showOrderBox");
			}
		}, function() { 
			window.location = window.location;
		}, "5.21");';
}
?>
		
		fullScreen = function() {
			if (typeof document.fullscreenEnabled !== undefined) {
				if (document.fullscreenElement) {
					document.getElementById("appletDiv").style.height = (clientHeight-35) + "px";
					document.getElementById("GameContent").style.height = clientHeight + "px";
					document.getElementById("GameContent").style.width = clientWidth + "px";
					document.getElementById("gameScreen").title = "Полный экран";
					document.getElementById("gameScreen").className = "b_box";
					document.exitFullscreen();
				} else {
					document.getElementById("appletDiv").style.height = (screenHeight-35) + "px";
					document.getElementById("GameContent").style.height = screenHeight + "px";
					document.getElementById("GameContent").style.width = screenWidth + "px";
					document.getElementById("gameScreen").title = "Обычный режим";
					document.getElementById("gameScreen").className = "m_box";
					document.getElementById('GameContent').requestFullscreen();
				}
			}
		}
		RestartGame = function(){
			if (typeof document.fullscreenEnabled !== undefined) {
				if (document.fullscreenElement) {
					document.exitFullscreen();	
				}
				window.location = <?php echo ($social ? "window.location" : "'http://www.allureonline.ru/index.html'" ); ?>;
			}
		}
		var _tmr = _tmr || [];
		_tmr.push({id: "2458081", type: "pageView", start: (new Date()).getTime()});
		(function (d, w) {
		   var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
		   ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
		   var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
		   if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
		})(document, window);
		document.getElementById("appletDiv").style.height = (clientHeight-35) + "px";
		document.getElementById("GameContent").style.height = clientHeight + "px";
	</script>
</body>
</html>